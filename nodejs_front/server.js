'use strict';

const express = require('express');
const https = require('https');

// Constants
const PORT = process.env.PORT || 8080;
const HOST = '0.0.0.0';
const API_HOST = process.env.API_HOST || 'node-api';
const API_PORT = process.env.API_PORT || 9090;

// App
const app = express();
app.get('/', (req, res) => {
   res.send('Received GET /');
});

app.get('/ping', (req, res) => {
   res.send('pong');
});

app.get('/consume-api', (req, res) => {
   res.send('Received GET /consume-api');

   https.get("http://${API_HOST}:${API_PORT}/api", (response) => {
     let todo = '';
   
     // called when a data chunk is received.
     response.on('data', (chunk) => {
       todo += chunk;
     });
   
     // called when the complete response is received.
     response.on('end', () => {
       console.log(todo);
     });
   
   }).on("error", (error) => {
     console.log("Error: " + error.message);
   });

});

app.listen(PORT, HOST);
console.log("Running on http://${HOST}:${PORT}");
