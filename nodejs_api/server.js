'use strict';

const express = require('express');

// Constants
const PORT = process.env.PORT || 8080;
const HOST = '0.0.0.0';
const SLEEP_MS = process.env.SLEEP_MS || 10000;

// sleep
const sleep = (milliseconds) => {
  return new Promise(resolve => setTimeout(resolve, milliseconds))
}

// App
const app = express();
app.get('/', (req, res) => {
   res.send('Received GET /');
});
app.get('/ping', (req, res) => {
   res.send('pong');
});

app.get('/api', (req, res) => {
   sleep(SLEEP_MS).then(() => {
       res.send("Received GET /api after sleeping ${SLEEP_MS}");
   })
});

app.listen(PORT, HOST);
console.log("Running on http://${HOST}:${PORT}");
